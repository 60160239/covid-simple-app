import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class QuestionWidget extends StatefulWidget {
  QuestionWidget({Key? key}) : super(key: key);

  @override
  _QuestionWidgetState createState() => _QuestionWidgetState();
}

class _QuestionWidgetState extends State<QuestionWidget> {
  var questionValue = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false
  ];
  var question = [
    'มีไข้หรือหนาวสั่น',
    'มีอาการไอ',
    'มีอาการแน่นหน้าอก',
    'มีอาการเหนื่อยล้า',
    'ปวดกล้ามเนื้อหรือร่างกาย',
    'ปวดหัว',
    'ไม่ได้กลิ่นหรือรส',
    'เจ็บคอ',
    'คัดจมูกน้ำมูกไหล',
    'คลื่นไส้อาเจียน',
    'ท้องเสีย'
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Question')),
      body: ListView(children: [
        CheckboxListTile(
          title: Text(question[0]),
          value: questionValue[0], 
          onChanged: (newValue){
            setState(() {
              questionValue[0] = newValue!;
            });
          }),
        CheckboxListTile(
          title: Text(question[1]),
          value: questionValue[1], 
          onChanged: (newValue){
            setState(() {
              questionValue[1] = newValue!;
            });
          }),
        CheckboxListTile(
          title: Text(question[2]),
          value: questionValue[2], 
          onChanged: (newValue){
            setState(() {
              questionValue[2] = newValue!;
            });
          }),
          CheckboxListTile(
          title: Text(question[3]),
          value: questionValue[3], 
          onChanged: (newValue){
            setState(() {
              questionValue[3] = newValue!;
            });
          }),
        CheckboxListTile(
          title: Text(question[4]),
          value: questionValue[4], 
          onChanged: (newValue){
            setState(() {
              questionValue[4] = newValue!;
            });
          }),
        CheckboxListTile(
          title: Text(question[5]),
          value: questionValue[5], 
          onChanged: (newValue){
            setState(() {
              questionValue[5] = newValue!;
            });
          }),
          CheckboxListTile(
          title: Text(question[6]),
          value: questionValue[6], 
          onChanged: (newValue){
            setState(() {
              questionValue[6] = newValue!;
            });
          }),
        CheckboxListTile(
          title: Text(question[7]),
          value: questionValue[7], 
          onChanged: (newValue){
            setState(() {
              questionValue[7] = newValue!;
            });
          }),
        CheckboxListTile(
          title: Text(question[8]),
          value: questionValue[8], 
          onChanged: (newValue){
            setState(() {
              questionValue[8] = newValue!;
            });
          }),
          CheckboxListTile(
          title: Text(question[9]),
          value: questionValue[9], 
          onChanged: (newValue){
            setState(() {
              questionValue[9] = newValue!;
            });
          }),
        CheckboxListTile(
          title: Text(question[10]),
          value: questionValue[10], 
          onChanged: (newValue){
            setState(() {
              questionValue[10] = newValue!;
            });
          }),
          ElevatedButton(
            onPressed: () async {
              await _saveQuestion();
              Navigator.pop(context);
            }, child: const Text('Save')),
          
      ],),
    );
  }

  @override
  void initState(){
    super.initState();
    _loadQuestion();
  }

  Future<void> _loadQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    var strQuestionValue = prefs.getString('question_value',)??"[false,false,false,false,false,false,false,false,false,false,false]";
    print(strQuestionValue.substring(1,strQuestionValue.length-1));
    var arrStrQuestionValue = strQuestionValue.substring(1,strQuestionValue.length-1).split(',');
    setState(() {
      for(var i=0;i<arrStrQuestionValue.length;i++){
        questionValue[i] = (arrStrQuestionValue[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('question_value',questionValue.toString());

  }
}