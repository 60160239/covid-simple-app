import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class VaccineWidget extends StatefulWidget {
  VaccineWidget({Key? key}) : super(key: key);

  @override
  _VaccineWidgetState createState() => _VaccineWidgetState();
}

class _VaccineWidgetState extends State<VaccineWidget> {
  var vaccine = ['-','-','-'];
  Widget _vaccineComboBox({required String title, required String value, ValueChanged<String?>? onChanged}){
    return Row(
            children: [
              Text(title),
              Expanded(child: Container()),
              DropdownButton(
                items: [
                  DropdownMenuItem(child: Text('-'), value: '-',),
                  DropdownMenuItem(child: Text('Pfizer'), value: 'Pfizer',),
                  DropdownMenuItem(child: Text('Johnson & Johnson'), value: 'Johnson & Johnson',),
                  DropdownMenuItem(child: Text('Sputnik'), value: 'Sputnik',),
                  DropdownMenuItem(child: Text('AstraZeneca'), value: 'AstraZeneca',),
                  DropdownMenuItem(child: Text('Novavax'), value: 'Novavax',),
                  DropdownMenuItem(child: Text('Sinopharm'), value: 'Sinopharm',),
                  DropdownMenuItem(child: Text('Sinovac'), value: 'Sinovac',),
                ],
                value: value,
                onChanged: onChanged,
              )
            ],
          );
  }
  Future<void> _loadVaccines() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
        vaccine = prefs.getStringList('vaccine')??['-','-','-'];
    });
  }

  @override
  void initState() {
    super.initState();
    _loadVaccines();
  }

  Future<void> _saveVaccine() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setStringList('vaccine', vaccine);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Vaccine'),),
      body: Container(
        padding: EdgeInsets.all(16),
        child:ListView(
        children: [
          _vaccineComboBox(title: 'เข็มที่ 1',value: vaccine[0],onChanged: (newValue) {
            setState(() {
              vaccine[0] = newValue!;
            });
          }),
          _vaccineComboBox(title: 'เข็มที่ 2',value: vaccine[1],onChanged: (newValue) {
            setState(() {
              vaccine[1] = newValue!;
            });
          }),
          _vaccineComboBox(title: 'เข็มที่ 3',value: vaccine[2],onChanged: (newValue) {
            setState(() {
              vaccine[2] = newValue!;
            });
          }),
          ElevatedButton(
            onPressed: (){
              _saveVaccine();
              Navigator.pop(context);
            },
            child: Text('Save'))
        ],
      )),
    );
  }
}