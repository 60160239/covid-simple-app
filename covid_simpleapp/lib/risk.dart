import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RiskWidget extends StatefulWidget {
  RiskWidget
({Key? key}) : super(key: key);

  @override
  _RiskWidgetState createState() => _RiskWidgetState();
}

class _RiskWidgetState extends State<RiskWidget> {
  var riskValue = [
    false,
    false,
    false,
    false,
    false,
    false,
    false
  ];
  var risk = [
    'โรคทางเดินหายใจเรื้อรัง',
    'โรคหัวใจและหลอดเลือด',
    'โรคไตวายเรื้อรัง',
    'โรคหลอดเลือดสมอง',
    'โรคอ้วน',
    'โรคมะเร็ง',
    'โรคเบาหวาน'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Risk'),),
      body: ListView(children: [
        CheckboxListTile(
          title: Text(risk[0]),
          value: riskValue[0], 
          onChanged: (newValue){
            setState(() {
              riskValue[0] = newValue!;
            });
        }),
        CheckboxListTile(
          title: Text(risk[1]),
          value: riskValue[1], 
          onChanged: (newValue){
            setState(() {
              riskValue[1] = newValue!;
            });
        }),
        CheckboxListTile(
          title: Text(risk[2]),
          value: riskValue[2], 
          onChanged: (newValue){
            setState(() {
              riskValue[2] = newValue!;
            });
        }),
        CheckboxListTile(
          title: Text(risk[3]),
          value: riskValue[3], 
          onChanged: (newValue){
            setState(() {
              riskValue[3] = newValue!;
            });
        }),
        CheckboxListTile(
          title: Text(risk[4]),
          value: riskValue[4], 
          onChanged: (newValue){
            setState(() {
              riskValue[4] = newValue!;
            });
        }),
        CheckboxListTile(
          title: Text(risk[5]),
          value: riskValue[5], 
          onChanged: (newValue){
            setState(() {
              riskValue[5] = newValue!;
            });
        }),
        CheckboxListTile(
          title: Text(risk[6]),
          value: riskValue[6], 
          onChanged: (newValue){
            setState(() {
              riskValue[6] = newValue!;
            });
        }),
        ElevatedButton(
          onPressed: () async {
            await _saveRisk();
            Navigator.pop(context);
          }, child: const Text('Save')
        ),
      ],),
    );
  }

  @override
  void initState(){
    super.initState();
    _loadRisk();
  }

  Future<void> _loadRisk() async {
    var prefs = await SharedPreferences.getInstance();
    var strRiskValue = prefs.getString('risk_value',)??"[false,false,false,false,false,false,false]";
    print(strRiskValue.substring(1,strRiskValue.length-1));
    var arrStrRiskValue = strRiskValue.substring(1,strRiskValue.length-1).split(',');
    setState(() {
      for(var i=0;i<arrStrRiskValue.length;i++){
        riskValue[i] = (arrStrRiskValue[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveRisk() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('risk_value',riskValue.toString());

  }
}